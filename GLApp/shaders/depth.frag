#version 330
//fragment shader

out float fragDepth;

void main (void)
{
	fragDepth = gl_FragCoord.z;
}