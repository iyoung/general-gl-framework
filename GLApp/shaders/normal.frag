#version 330
//fragment shader

out vec4 colour;

uniform vec3 meshColour;

void main (void)
{
	colour = vec4(meshColour,1.0);
}