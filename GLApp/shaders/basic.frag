#version 330
//fragment shader

out vec4 colour;

uniform vec3 meshColour;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec4 direction;
	vec3 attenuation;
	float cutoff;
	int lightType;
};

uniform lightStruct light;

in VS_FS_Block
{
	vec3 transformedNormal;
	vec3 light_Dir;
	vec3 vVec;
	vec2 coord;
	vec3 LF_Dir;
	vec4 shadowCoord;
} data;

uniform sampler2DShadow shadowMap;

void main (void)
{
	vec2 poissonDisk[4] = vec2[](
	vec2( -0.94201624, -0.39906216 ),
	vec2( 0.94558609, -0.76890725 ),
	vec2( -0.094184101, -0.92938870 ),
	vec2( 0.34495938, 0.29387760 ) );

	float nDotL = max(0.0,dot( data.transformedNormal, normalize( data.light_Dir ) ) );
	float depth = 1.0;
	float bias = 0.005*tan( acos( nDotL ) );
	//poisson disk sampling. If the fragment depth is greater than the depth sampled from the shadow map (in light view space)
	//then it is in shadow, we use the poisson disk to blur the shadow
	for (int i=0;i<4;i++)
	{
		if ( texture( shadowMap, vec3(data.shadowCoord.xy+poissonDisk[i]/700.0, (data.shadowCoord.z)/data.shadowCoord.w))  <  data.shadowCoord.z-bias)
		{
			depth-=0.20;
		}
	}
	//depth = texture( shadowMap, vec3(data.shadowCoord.xy, (data.shadowCoord.z-bias)/data.shadowCoord.w));
	vec3 tmpCol  =  depth * light.ambient.rgb * meshColour;
	
	if (nDotL > 0.0)
	{
		//compare depth to fragment position
		if ( light.lightType == 0)
		{
			tmpCol += depth * meshColour.rgb * light.diffuse.rgb * max( nDotL, 0 );
		}
		if ( light.lightType == 1)
		{
			float len = length( data.light_Dir );
			float att = light.attenuation.x/((1.0+light.attenuation.y*len)*(1.0+light.attenuation.z*len*len));
			tmpCol += meshColour.rbg * light.diffuse.rgb * max( nDotL, 0 ) * (1.0/len);
			vec3 R = normalize( -reflect( data.light_Dir, data.transformedNormal ) );
			tmpCol += depth * meshColour.rgb * light.specular.xyz * pow( max( dot( R, data.vVec ), 0 ), 1.0 );
			tmpCol*=att;
		}
		if ( light.lightType == 2)
		{
			nDotL = dot( data.transformedNormal, normalize( data.LF_Dir ) );
			float spot = dot( normalize(data.light_Dir), normalize( data.LF_Dir ) );
			if( spot > light.cutoff )
			{
				float len = length(data.LF_Dir);
				float att = spot/(1.0/(light.attenuation.x+((1.0+light.attenuation.y*len)*(1.0+light.attenuation.z*len*len))));
				tmpCol += depth * meshColour.rgb * light.diffuse.rgb * max( dot( data.transformedNormal, normalize( data.light_Dir ) ), 0 );
				vec3 R = normalize( -reflect( data.light_Dir, data.transformedNormal ) );
				tmpCol += depth * meshColour.rgb * light.specular.xyz * pow( max( dot( R, data.vVec ), 0 ), 255 );
				tmpCol *= att;
			}
		}
	}
	colour = vec4( tmpCol,1.0 );
}