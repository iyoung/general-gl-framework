#pragma once
#include <glm\glm.hpp>
#include <vector>
#include <memory>
#include "Material.h"

enum ModelAttribute
{
    BUFFER_VERTEX,
    BUFFER_NORMAL,
    BUFFER_TEX_COORD,
    BUFFER_INDEX,
    NUM_BUFFERS
};

class Model
{
public:
    Model(void);
    virtual int load(const char* pFileName);
    virtual int loadIndexed(const char* pFileName);
    int getVertexCount();
    unsigned int getModelHandle();
    void setColour(const glm::vec3& pColour);
	void setMaterial(const std::shared_ptr<Material> pMaterial);
	std::shared_ptr<Material> getMaterial();
	bool hasMaterial();
    bool isIndexed(){ return mIndexed;  }
    glm::vec3 getColour();
    virtual ~Model(void);
protected:
    std::vector<glm::vec3> mVertices;
    std::vector<glm::vec3> mNormals;
    std::vector<glm::vec2> mTexCoords;
    std::vector<unsigned int> mIndices;
    unsigned int mVAO;
    unsigned int mVBO[NUM_BUFFERS];
    unsigned int mIndexCount;
    glm::vec3 mColour;
	std::shared_ptr<Material> mMaterial;
    bool mIndexed;
};

