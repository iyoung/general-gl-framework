#include "SceneObjectFactory.h"
#include "SceneObject.h"

SceneObjectFactory::SceneObjectFactory(void)
{
}

std::shared_ptr<SceneObject> SceneObjectFactory::createObject()
{
    auto object = std::make_shared<SceneObject>();

    mObjects.push_back( object );
    object->setID( mObjects.size() - 1 );

    return object;
}

void SceneObjectFactory::releaseObjects()
{
    mObjects.clear();
}

SceneObjectFactory::~SceneObjectFactory(void)
{
    releaseObjects();
}
