#include "GLApp.h"
#include "Timer.h"
#include "SceneManager.h"
#include "FileIO.h"
#include "SceneObjectFactory.h"
#include "SceneObject.h"
#include "Model.h"
#include "Camera.h"
#include "Controller.h"
#include "Light.h"
#include <glm\gtc\matrix_transform.hpp>
#include <GL\glew.h>
#include "Renderer.h"
#include <glm\gtx\rotate_vector.hpp>
#include <glm\gtc\quaternion.hpp>

GLApp::GLApp(void):mRunning(false),mTimer(nullptr)
{
    mWindow = nullptr;
    mCamera = nullptr;
    mLight = nullptr;
    mLightMod = 0;
}
int GLApp::init(int pWinWidth, int pWinHeight)
{
    mTimer = std::unique_ptr<Timer>( new Timer() );
    printf("Timer: OK!\n");

    mRenderer = std::unique_ptr<Renderer>( new Renderer() );
    if ( mRenderer->init(pWinWidth, pWinHeight) < 0)
    {
        printf("renderer error \n");
        return -1;
    }

    //create camera, and initialize basic data
    mCamera = std::make_shared<Camera>();
	mCamera->Reset();
    mCamera->setViewPort( pWinWidth, pWinHeight );
    mCamera->setDrawDistance( 512.0f );
    mCamera->setFieldOfView( 120.0f );
    mCamera->MoveTo( glm::vec3( 0.0, 200.0f, -200.0f ) );
	mCamera->RotatePitch(-10.0f);
    mCamera->RotateYaw(-99.0f);
    mCamera->LookAt(glm::vec3(0.0f,2.0f,0.0f),250.0f);
    mRenderer->addCamera(mCamera);


	//create a scene object factory to construct scene objects
    mObjectFactory = std::unique_ptr< SceneObjectFactory >( new SceneObjectFactory() );
    //create scene.
    //load a plane for ground.
    auto object1 = mObjectFactory->createObject();
    object1->activate();
    auto mesh1 = std::make_shared< Model >();
    object1->setPosition( glm::vec3( 0.0, 0.0, 0.0f ) );
    object1->setScale( glm::vec3( 1.0f, 1.0f, 1.0f ) );
    if(mesh1->load( "models/room_thickwalls.obj" ) > -1 )
        object1->setModel(mesh1);
    mObjects.push_back( object1 );
    mesh1->setColour( glm::vec3( 1.0, 1.0, 1.0 ) );
    //object1->rotate(YAW,180.0f);
    mRenderer->addSceneObject(object1);
    //create a second scene object to cast the shadow.
    //auto object2 = mObjectFactory->createObject();
    //object2->activate();
    //auto mesh2 = std::make_shared< Model >();
    //object2->setPosition( glm::vec3( 0.0f , 2.2f ,6.0f) );
    //object2->setScale( glm::vec3( 7.0f, 2.0f, 0.5f ) );

    //if( mesh2->load("models/cube-flat.obj") > -1 )
    //    object2->setModel( mesh2 );
    //mObjects.push_back( object2 );
    //mesh2->setColour( glm::vec3( 0.0f, 1.0f, 0.0f ) );
	//create and initialize shadow buffer

	//create a light for the scene
    mLight = std::make_shared<Light>();
    mLight->setAmbient( glm::vec4(0.02f,0.02f,0.02f,1.0f) );
    mLight->setDiffuse( glm::vec4(0.7f,0.7f,0.7f,1.0f) );
    mLight->setSpecular( glm::vec4(1.0f,1.0f,1.0f,1.0f) );
	mLight->setPosition( glm::vec4(2.0f,4.0f,4.0f,1.0f) );
    mLight->setLightType( DIRECTIONAL );
    mLight->setDirection( glm::vec4(0.5f,2.0f,2.0f,0.0f) );
	mLight->setCutOffAngle( 45.0f );
	mLight->setAttenuation(glm::vec3(1.0f,0.22f,0.2f));
    mRenderer->addLight(mLight);
	//create a controller object
    mController = std::unique_ptr< Controller >( new Controller () );

    mRunning = true;
    printf("CONTROLS:\n");
    printf("WASD, SPACE, CTRL: Move camera\n");
    printf("Mouse rotates camera\n");
    printf("Enter: rotate room 180 degrees\n");

    return 0;
}
void GLApp::run()
{
    while (mRunning)
    {
        //update timer
        mTimer->update();
        //parse input events
        //update scene manager
        SDL_Event sdlEvent;
        while ( SDL_PollEvent( &sdlEvent ) )
        {
            mRunning = this->handleEvent(sdlEvent);
        }
        update(mTimer->timeSinceLast());
        //if timer output is greater than minimum timestep
        renderScene();
    }
}
void GLApp::exitFatalError(const char* pErrorMsg)
{
    FILEIO::FileIO::saveFile("error_log.txt",pErrorMsg);
    printf("App level error. see log file\n");
}
void GLApp::shutDown()
{
    mRunning = false;
    mRenderer->shutDown();
}
bool GLApp::handleEvent(SDL_Event sdlEvent)
{
    if( sdlEvent.type == SDL_QUIT)
        return false;
    if( sdlEvent.key.keysym.scancode == SDL_SCANCODE_ESCAPE )
        return false;
    mController->update(sdlEvent);
    return true;
}
void GLApp::update(const float& pDeltaTimeS)
{
    float speed = 5.0f;

    //keyboard camera control
    if(mController->getKeyHeld(FORWARD))
        mCamera->MoveForward(speed*pDeltaTimeS);
    if(mController->getKeyHeld(BACKPEDAL))
        mCamera->MoveForward(-speed*pDeltaTimeS);
    if(mController->getKeyHeld(STRAFE_L))
        mCamera->MoveRight(-speed*pDeltaTimeS);
    if(mController->getKeyHeld(STRAFE_R))
        mCamera->MoveRight(speed*pDeltaTimeS);
    if(mController->getKeyHeld(JUMP))
        mCamera->MoveUp(speed*pDeltaTimeS);
    if(mController->getKeyHeld(CROUCH))
        mCamera->MoveUp(-speed*pDeltaTimeS);

    //mouse camera control
    if(mController->getMouseMovementX()<0)
        mCamera->RotateYaw(speed*pDeltaTimeS);
    if(mController->getMouseMovementX()>0)
        mCamera->RotateYaw(-speed*pDeltaTimeS);
    if(mController->getMouseMovementY()<0)
        mCamera->RotatePitch(speed*pDeltaTimeS);
    if(mController->getMouseMovementY()>0)
        mCamera->RotatePitch(-speed*pDeltaTimeS);

    //option to move light: only works in spotlight or point light mode
	if(mController->getKeyHeld(ASCEND))
        mLight->move( glm::vec3(mLight->getData().mDirection)*pDeltaTimeS );
	if(mController->getKeyHeld(DESCEND))
        mLight->move( -glm::vec3(mLight->getData().mDirection)*pDeltaTimeS );

    //option to rotate room
    if(mController->getKeyReleased(FULLSCREEN_KEY_A))
        mObjects[0]->rotate(YAW,180.0f);
    if(mController->getKeyReleased(PAUSE))
        if(mLightMod < 2)
            mLightMod++;
        else
            mLightMod = 0;

    if(mLightMod == 0)
        mLight->setLightType(DIRECTIONAL);
    else if(mLightMod == 1)
        mLight->setLightType(SPOT_LIGHT);
    else if(mLightMod == 2)
        mLight->setLightType(POINT_LIGHT);

    auto iter = mObjects.begin();
    auto end = mObjects.end();
    for ( ; iter!=end ; iter++ )
    {
        (*iter)->update(pDeltaTimeS);
    }
    mObjects[0]->rotate(YAW,speed*pDeltaTimeS);
    mCamera->RotateYaw(speed*pDeltaTimeS);
    mCamera->LookAt(glm::vec3(0.0f,2.0f,0.0f),250.0f);

    mController->update();
}
void GLApp::renderScene()
{
    mRenderer->renderScene();
}
GLApp::~GLApp(void)
{
    mObjects.clear();
}
