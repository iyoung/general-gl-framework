#include "Shader.h"
#include <glm\gtc\type_ptr.hpp>
#include "FileIO.h"

Shader::Shader(void)
{
    mShaderProgram = 0;
}
int Shader::load(const char* pVertex, const char* pFragment)
{
    GLuint p, f, v;

	char *vs,*fs;

	v = glCreateShader(GL_VERTEX_SHADER);
	f = glCreateShader(GL_FRAGMENT_SHADER);

	// load shaders & get length of each
	GLint vlen;
	GLint flen;
    vs = FILEIO::FileIO::loadFile(pVertex,vlen);
	fs = FILEIO::FileIO::loadFile(pFragment,flen);
	
	const char * vv = vs;
	const char * ff = fs;

	glShaderSource(v, 1, &vv,&vlen);
	glShaderSource(f, 1, &ff,&flen);
	
	GLint compiled;

	glCompileShader(v);
	glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		printShaderError(v);
        return -1;
	} 

	glCompileShader(f);
	glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		printShaderError(f);
        return -1;
	} 
	
	p = glCreateProgram();
		
	glAttachShader(p,v);
	glAttachShader(p,f);

	glBindAttribLocation(p,VERTEX,"in_Position");
	glBindAttribLocation(p,NORMAL,"in_Normal");
	glBindAttribLocation(p,TEX_COORD,"in_TexCoord");



	glLinkProgram(p);

	glUseProgram(p);
    mModelViewUniform = glGetUniformLocation(p, "ModelView");
    mProjectionUniform = glGetUniformLocation(p, "Projection");
	delete [] vs; // dont forget to free allocated memory
	delete [] fs; // we allocated this in the loadFile function...

    mShaderProgram = p;
    return 0;
}
void Shader::activate()
{
    glUseProgram(mShaderProgram);
}
void Shader::deactivate()
{
    glUseProgram(0);
}
void Shader::setModelView(const glm::mat4& pMatrix)
{
    glUniformMatrix4fv(mModelViewUniform, 1, GL_FALSE, glm::value_ptr(pMatrix));
}
void Shader::setProjection(const glm::mat4& pMatrix)
{
    glUniformMatrix4fv(mProjectionUniform, 1, GL_FALSE, glm::value_ptr(pMatrix));
}
void Shader::setGLM_Matrix(const glm::mat4& pMatrix, const char* pUniformName)
{
    int uniformIndex = glGetUniformLocation(mShaderProgram, pUniformName);
    glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, glm::value_ptr(pMatrix)); 
}
void Shader::setGLM_Matrix(const glm::mat3& pMatrix, const char* pUniformName)
{
    int uniformIndex = glGetUniformLocation(mShaderProgram, pUniformName);
    glUniformMatrix3fv(uniformIndex, 1, GL_FALSE, glm::value_ptr(pMatrix)); 
}
void Shader::setGLM_Vector(const glm::vec3& pVector, const char* pUniformName)
{
    int uniformIndex = glGetUniformLocation(mShaderProgram, pUniformName);
    glUniform3fv(uniformIndex, 1, glm::value_ptr(pVector));
}
void Shader::setGLM_Vector(const glm::vec4& pVector, const char* pUniformName)
{
    int uniformIndex = glGetUniformLocation(mShaderProgram, pUniformName); 
    glUniform4fv(uniformIndex, 1, glm::value_ptr(pVector));
}
void Shader::printShaderError(GLuint pShader)
{
    int maxLength = 0;
	int logLength = 0;
	GLchar *logMessage;

	// Find out how long the error message is
	if (!glIsShader(pShader))
		glGetProgramiv(pShader, GL_INFO_LOG_LENGTH, &maxLength);
	else
		glGetShaderiv(pShader, GL_INFO_LOG_LENGTH, &maxLength);

	if (maxLength > 0) { // If message has some contents
		logMessage = new GLchar[maxLength];
		if (!glIsShader(pShader))
			glGetProgramInfoLog(pShader, maxLength, &logLength, logMessage);
		else
			glGetShaderInfoLog(pShader,maxLength, &logLength, logMessage);
        FILEIO::FileIO::saveFile("shaders/shader_log.txt",logMessage);
		delete [] logMessage;
	}
	// should additionally check for OpenGL errors here
}
void Shader::setLight(LightData& light)
{
    int location = glGetUniformLocation(mShaderProgram,"light.ambient");
    glUniform4fv(location, 1, glm::value_ptr(light.mAmbient));

    location = glGetUniformLocation(mShaderProgram,"light.diffuse");
    glUniform4fv(location, 1, glm::value_ptr(light.mDiffuse));

    location = glGetUniformLocation(mShaderProgram,"light.specular");
    glUniform4fv(location, 1, glm::value_ptr(light.mSpecular));

	location = glGetUniformLocation(mShaderProgram,"light.position");
    glUniform4fv(location, 1, glm::value_ptr(light.mPosition));

    location = glGetUniformLocation(mShaderProgram,"light.direction");
    glUniform4fv(location, 1, glm::value_ptr(light.mDirection));

	location = glGetUniformLocation(mShaderProgram,"light.attenuation");
	glUniform3fv(location, 1, glm::value_ptr(light.mAttenuation));

    location = glGetUniformLocation(mShaderProgram,"light.cutoff");
    glUniform1f(location,light.mCutOffAngle);

    location = glGetUniformLocation(mShaderProgram,"light.lightType");
    glUniform1i(location,light.mLightType);
}
void Shader::setMaterial(Material& pMaterial)
{
	int location = glGetUniformLocation(mShaderProgram,"material.ambient");
	glUniform4fv(location, 1, glm::value_ptr(pMaterial.getAmbient()));

    location = glGetUniformLocation(mShaderProgram,"material.diffuse");
	glUniform4fv(location, 1, glm::value_ptr(pMaterial.getDiffuse()));

    location = glGetUniformLocation(mShaderProgram,"material.specular");
	glUniform4fv(location, 1, glm::value_ptr(pMaterial.getSpecular()));

	location = glGetUniformLocation(mShaderProgram,"material.shininess");
	glUniform1f(location,pMaterial.getShininess());
}

Shader::~Shader(void)
{
    glDeleteShader(mShaderProgram);
}
