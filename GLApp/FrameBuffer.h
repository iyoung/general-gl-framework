#pragma once
#include <GL\glew.h>
class FrameBuffer
{
public:
    FrameBuffer(void);
    virtual int init(int pWidth, int pHeight);
    void BindRead(GLenum pTextureUnit);
    void BindWrite();
	void Unbind();
    virtual ~FrameBuffer(void);
protected:
    GLuint m_fbo;
    GLuint m_shadowMap;
};

