#pragma once
#include <memory>
#include <vector>
#include <SDL.h>
#include <GL\glew.h>
#include "FrameBuffer.h"
#include "Shader.h"
#include "SceneObject.h"
#include "Light.h"
#include <glm\glm.hpp>
#include "Camera.h"

class Renderer
{
public:
    Renderer(void);
    virtual int init(const int& pWinWidth, const int& pWinHeight);
    virtual void renderScene();
    void addSceneObject(std::shared_ptr<SceneObject> pObject);
    void addLight(std::shared_ptr<Light> pLight);
    void addCamera(std::shared_ptr<Camera> pCam);
    void shutDown();
    virtual ~Renderer(void);
protected:
    int openWindow(const int& pWinWidth, const int& pWinHeight);
    void depthRender();
    std::vector<std::shared_ptr<SceneObject>> mObjects;
    std::shared_ptr<Light> mLight;
    SDL_Window* mWindow;
    SDL_GLContext mContext;
    std::unique_ptr<FrameBuffer> mShadowBuffer;
    std::unique_ptr<Shader> mNormalShader;
    std::unique_ptr<Shader> mDepthShader;
    std::shared_ptr<Camera> mCamera;
    glm::mat4 shadowMatrix;
    int mWindowHeight;
    int mWindowWidth;
};

