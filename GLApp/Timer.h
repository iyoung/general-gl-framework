#pragma once
class Timer
{
public:
    Timer(void);
    void update();
    const float& timeSinceLast();
    const float& totalElapsed();
    ~Timer(void);
private:
    float mLastTime;
    float mTotalTime;
};

