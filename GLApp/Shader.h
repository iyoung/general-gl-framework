#pragma once
#include <GL\glew.h>
#include <glm\glm.hpp>
#include "Light.h"
#include "Material.h"
enum ShaderAttribute
{
    VERTEX,
    NORMAL,
    TEX_COORD,
    COLOUR
};

class Shader
{
public:
    Shader(void);
    int load(const char* pVertex, const char* pFragment);
    void activate();
    void deactivate();
    void setModelView(const glm::mat4& pMatrix);
    void setProjection(const glm::mat4& pMatrix);
    void setGLM_Matrix(const glm::mat4& pMatrix, const char* pUniformName);
    void setGLM_Matrix(const glm::mat3& pMatrix, const char* pUniformName);
    void setGLM_Vector(const glm::vec3& pVector, const char* pUniformName);
    void setGLM_Vector(const glm::vec4& pVector, const char* pUniformName);
    void setLight(LightData& pLight);
	void setMaterial(Material& pMaterial);
    void printShaderError(GLuint pShader);
    virtual ~Shader(void);

protected:
    GLuint mShaderProgram;
    GLuint mModelViewUniform;
    GLuint mProjectionUniform;
};

