#include "Light.h"


Light::Light(void)
{
}

void Light::setAmbient(const glm::vec4& pAmbient)
{
    mData.mAmbient = pAmbient;
}

void Light::setDiffuse(const glm::vec4& pDiffuse)
{
    mData.mDiffuse = pDiffuse;
}

void Light::setSpecular(const glm::vec4& pSpecular)
{
    mData.mSpecular = pSpecular;
}

void Light::setPosition(const glm::vec4& pPosition)
{
    mData.mPosition = pPosition;
}

void Light::setDirection(const glm::vec4& pDirection)
{
    mData.mDirection = pDirection;
}

void Light::setCutOffAngle(const float& pAngle)
{
	mData.mCutOffAngle = glm::radians(pAngle);
}

void Light::setLightType(const LightType& pType)
{
    mData.mLightType = (int)pType;
}
void Light::move(const glm::vec3& pOffset)
{
	mData.mPosition+=glm::vec4(pOffset,0.0f);
}
void Light::setAttenuation(const glm::vec3& pAttenuation)
{
	mData.mAttenuation = pAttenuation;
}

LightData& Light::getData()
{
    return mData;
}

Light::~Light(void)
{
}
