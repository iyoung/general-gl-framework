#include "Model.h"
#include <GL\glew.h>
#include <algorithm>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>


Model::Model(void):mIndexCount(0),mVAO(0),mMaterial(nullptr)
{
}
int Model::loadIndexed(const char* pFileName)
{
    std::string line;
	std::ifstream inFile( pFileName, std::ios_base::in );
    bool hasTexture=false;
	bool smoothShading;
    std::vector<glm::vec2> tex;
    std::vector<int> normIDs;
	std::vector<int> texID;
    std::vector<glm::vec3> norms;

	if(!inFile.is_open())
	{
		std::cout << "file not opened" << std::endl;
		return -1;
	}
    while(!inFile.eof())
    {
        char buff[255];
		inFile.getline(buff,255);
		line.assign(buff);

        //organise data
 		if(line.c_str()[0] == '#')
		{
			continue;
		}
        //read vertices
		if(line.substr(0,2) == "v ")
		{
			std::istringstream s(line.substr(2));
			glm::vec3 v;
			s>>v.x;s>>v.y;s>>v.z;
            mVertices.push_back(v);
		}

		if(line.substr(0,3) == "vn ")
		{
			std::istringstream s(line.substr(2));
			glm::vec3 v;
			s>>v.x;s>>v.y;s>>v.z;
			norms.push_back(v);
		}

        if(line.substr(0,3) == "vt ")
		{
			hasTexture=true;
			std::istringstream s(line.substr(2));
			glm::vec2 v;
			s>>v.x;s>>v.y;
            tex.push_back(v);
		}

        if(line.substr(0,3) == "s 1")
		{
			smoothShading=true;
        }

        		//also will need to be updated to handle smooth shading off "s off" scenario
		if(line.substr(0,2) == "f ")
		{
			std::istringstream stream(line.substr(2));
			
			if(hasTexture)
			{
				int v1,v2,v3,n1,n2,n3,t1,t2,t3;
				char tmp;
				//parse vertices & normals to indices, and normalIndices
				//using indices, populate ordered lists of normals and verts
				//parse, using tmp to filter out '/' delimiters
				stream>>v1>>tmp>>t1>>tmp>>n1>>v2>>tmp>>t2>>tmp>>n2>>v3>>tmp>>t3>>tmp>>n3;
				//set all vars to use c++ array indices
				v1--;v2--;v3--;n1--;n2--;n3--;t1--;t2--;t3--;
				mIndices.push_back(v1);
				mIndices.push_back(v2);
				mIndices.push_back(v3);
				texID.push_back(t1);
				texID.push_back(t2);
				texID.push_back(t3);
				normIDs.push_back(n1);
				normIDs.push_back(n2);
				normIDs.push_back(n3);
			}
			else
			{
				int v1,v2,v3,n1,n2,n3;
				char tmp;
				//parse vertices & normals to indices, and normalIndices
				//using indices, populate ordered lists of normals and verts
				//parse, using tmp to filter out '/' delimiters
				stream>>v1>>tmp>>tmp>>n1>>v2>>tmp>>tmp>>n2>>v3>>tmp>>tmp>>n3;
				//set all vars to use c++ array indices
				v1--;v2--;v3--;n1--;n2--;n3--;
				mIndices.push_back(v1);
				mIndices.push_back(v2);
				mIndices.push_back(v3);
				normIDs.push_back(n1);
				normIDs.push_back(n2);
				normIDs.push_back(n3);
			}
		}
    }
    //now sort out the final data format from the face data
  	for (size_t i = 0; i < normIDs.size();i++)
	{
        mNormals.push_back(norms[normIDs[i]]);
	}
	for (size_t i =0;i<texID.size();i++)
	{
		mTexCoords.push_back(tex[texID[i]]);
	}

    //now send data to GPU
    glGenVertexArrays(1,&mVAO);
    glBindVertexArray(mVAO);

    glGenBuffers(NUM_BUFFERS,mVBO);

    if(!mVertices.empty())
    {
        glBindBuffer(GL_ARRAY_BUFFER,mVBO[BUFFER_VERTEX]);
        glBufferData(GL_ARRAY_BUFFER,mVertices.size()*sizeof(glm::vec3),mVertices.data(),GL_STATIC_DRAW);
        glVertexAttribPointer((GLuint)BUFFER_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(BUFFER_VERTEX);
    }
    if(!mNormals.empty())
    {
        glBindBuffer(GL_ARRAY_BUFFER,mVBO[BUFFER_NORMAL]);
        glBufferData(GL_ARRAY_BUFFER,mNormals.size()*sizeof(glm::vec3),mNormals.data(),GL_STATIC_DRAW);
        glVertexAttribPointer((GLuint)BUFFER_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(BUFFER_NORMAL);
    }
    if(!mTexCoords.empty())
    {
        glBindBuffer(GL_ARRAY_BUFFER,mVBO[BUFFER_TEX_COORD]);
        glBufferData(GL_ARRAY_BUFFER,mTexCoords.size()*sizeof(glm::vec2),mTexCoords.data(),GL_STATIC_DRAW);
        glVertexAttribPointer((GLuint)BUFFER_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(BUFFER_TEX_COORD);
    }
    if(!mIndices.empty())
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,mVBO[BUFFER_INDEX]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,mIndices.size()*sizeof(unsigned int),mIndices.data(),GL_STATIC_DRAW);
    }

    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);

    //clean up date
    mVertices.clear();
    mTexCoords.clear();
    mNormals.clear();
    tex.clear();
    normIDs.clear();
    texID.clear();
    norms.clear();
    mIndexCount = mIndices.size();
    mIndices.clear();
    mIndexed = true;
    return 0;
}
int Model::load(const char* pFileName)
{
    std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
    std::vector< glm::vec3 > temp_vertices;
    std::vector< glm::vec2 > temp_uvs;
    std::vector< glm::vec3 > temp_normals;

    std::vector< glm::vec3 > out_vertices;
    std::vector< glm::vec2 > out_uvs;
    std::vector< glm::vec3 > out_normals;

    FILE * file = fopen(pFileName, "r");
    if( file == NULL )
    {
        printf("Impossible to open the file !\n");
        return -1;
    }

    while( 1 )
    {
        char lineHeader[128];
        // read the first word of the line
        int res = fscanf(file, "%s", lineHeader);
        if (res == EOF)
            break; // EOF = End Of File. Quit the loop.
 
        // else : parse lineHeader
        if ( strcmp( lineHeader, "v" ) == 0 )
        {
            glm::vec3 vertex;
            fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z );
            temp_vertices.push_back(vertex);
        }
        else if ( strcmp( lineHeader, "vt" ) == 0 )
        {
            glm::vec2 uv;
            fscanf(file, "%f %f\n", &uv.x, &uv.y );
            temp_uvs.push_back(uv);
        }
        else if ( strcmp( lineHeader, "vn" ) == 0 )
        {
            glm::vec3 normal;
            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z );
            temp_normals.push_back(normal);
        }
        else if ( strcmp( lineHeader, "f" ) == 0 )
        {
            std::string vertex1, vertex2, vertex3;
            unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
            int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2] );
            if (matches != 9)
            {
                printf("File can't be read by our simple parser :  Try exporting with other options\n");
                return -1;
            }
            vertexIndices.push_back(vertexIndex[0]);
            vertexIndices.push_back(vertexIndex[1]);
            vertexIndices.push_back(vertexIndex[2]);
            uvIndices    .push_back(uvIndex[0]);
            uvIndices    .push_back(uvIndex[1]);
            uvIndices    .push_back(uvIndex[2]);
            normalIndices.push_back(normalIndex[0]);
            normalIndices.push_back(normalIndex[1]);
            normalIndices.push_back(normalIndex[2]);
        }
    }

    // For each vertex of each triangle
    for( unsigned int i=0; i<vertexIndices.size(); i++ )
    {
        unsigned int vertexIndex = vertexIndices[i];
        glm::vec3 vertex = temp_vertices[ vertexIndex-1 ];
        out_vertices.push_back(vertex);

        unsigned int uvIndex = uvIndices[i];
        glm::vec2 uv = temp_uvs[ uvIndex-1 ];
        out_uvs.push_back(uv);

        unsigned int normalIndex = normalIndices[i];
        glm::vec3 normal = temp_normals[ normalIndex-1 ];
        out_normals.push_back(normal);
    }
    //now send data to GPU
    glGenVertexArrays(1,&mVAO);
    glBindVertexArray(mVAO);

    glGenBuffers(NUM_BUFFERS,mVBO);

    if(!out_vertices.empty())
    {
        glBindBuffer(GL_ARRAY_BUFFER,mVBO[BUFFER_VERTEX]);
        glBufferData(GL_ARRAY_BUFFER,out_vertices.size()*sizeof(glm::vec3),out_vertices.data(),GL_STATIC_DRAW);
        glVertexAttribPointer((GLuint)BUFFER_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(BUFFER_VERTEX);
    }
    if(!out_normals.empty())
    {
        glBindBuffer(GL_ARRAY_BUFFER,mVBO[BUFFER_NORMAL]);
        glBufferData(GL_ARRAY_BUFFER,out_normals.size()*sizeof(glm::vec3),out_normals.data(),GL_STATIC_DRAW);
        glVertexAttribPointer((GLuint)BUFFER_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(BUFFER_NORMAL);
    }
    if(!out_uvs.empty())
    {
        glBindBuffer(GL_ARRAY_BUFFER,mVBO[BUFFER_TEX_COORD]);
        glBufferData(GL_ARRAY_BUFFER,out_uvs.size()*sizeof(glm::vec2),out_uvs.data(),GL_STATIC_DRAW);
        glVertexAttribPointer((GLuint)BUFFER_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(BUFFER_TEX_COORD);
    }
    mIndexed = false;
    mIndexCount = out_vertices.size();
    out_vertices.clear();
    out_normals.clear();
    out_uvs.clear();
    temp_normals.clear();
    temp_uvs.clear();
    temp_vertices.clear();
    
    return 0;
}
unsigned int Model::getModelHandle()
{
    return mVAO;
}
int Model::getVertexCount()
{
    return mIndexCount;
}
void Model::setColour(const glm::vec3& pColour)
{
    mColour = pColour;
}
glm::vec3 Model::getColour()
{
    return mColour;
}
void Model::setMaterial(std::shared_ptr<Material> pMaterial)
{
	mMaterial = pMaterial;
}
bool Model::hasMaterial()
{
	return (mMaterial != nullptr);
}
std::shared_ptr<Material> Model::getMaterial()
{
	return mMaterial;
}
Model::~Model(void)
{
    glDeleteBuffers(NUM_BUFFERS,mVBO);
    glDeleteVertexArrays(1,&mVAO);
}
