#include "Timer.h"
#include <ctime>

Timer::Timer(void):mLastTime(0.0f), mTotalTime(0.0f)
{
}

void Timer::update()
{
    float current = (float)clock()/CLOCKS_PER_SEC;

    mLastTime  = current - mTotalTime;

    mTotalTime = current;
}

const float& Timer::timeSinceLast()
{
    return mLastTime;
}

const float& Timer::totalElapsed()
{
    return mTotalTime;
}

Timer::~Timer(void)
{
}
